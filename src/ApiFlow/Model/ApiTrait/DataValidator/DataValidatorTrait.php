<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataValidator;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Exception\ValidatorException;

use Th20\ApiFlow\Model\ApiInterface\DataValidatorInterface;

/**
 * A trait implementation of DataValidator for controller classes.
 */
trait DataValidatorTrait
{

    /**
     * Implements DataValidatorInterface::isDataValid().
     */
    public function isDataValid($data)
    {
        $validator = $this->get('validator');
        $errors = $validator->validate($data);

        return count($errors) == 0;
    }

    /**
     * Implements DataValidatorInterface::requireDataValid().
     */
    public function requireDataValid($data)
    {
        if (!$this->isDataValid($data)) {
            $message = 'Data does not pass constraints validation.';
            $exception = new ValidatorException($message);

            throw new BadRequestHttpException($message, $exception);
        }
    }

}
