<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataPersister;

use Th20\ApiFlow\Model\ApiInterface\DataAccessCheckerInterface;
use Th20\ApiFlow\Model\ApiInterface\DataPersisterInterface;
use Th20\ApiFlow\Model\ApiInterface\DataValidatorInterface;

/**
 * A trait implementation of DataPersisterInterface for controller classes.
 */
trait DoctrineDataPersisterTrait
{

    /**
     * Implements DataPersisterInterface::persistData().
     */
    public function persistData($data)
    {
        return $this->persistDataSet(array($data));
    }

    /**
     * Implements DataPersisterInterface::persistDataSet().
     */
    public function persistDataSet(array $dataSet)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $unitOfWork = $entityManager->getUnitOfWork();

        foreach ($dataSet as $data) {
            if ($this instanceof DataValidatorInterface) {
                $this->requireDataValid($data);
            }

            $entityManager->persist($data);

            if ($unitOfWork->isScheduledForInsert($data)) {
                if ($this instanceof DataAccessCheckerInterface) {
                    $this->requireDataCreateAccessGranted($data);
                }
            } else {
                if ($this instanceof DataAccessCheckerInterface) {
                    $this->requireDataUpdateAccessGranted($data);
                }
            }
        }

        $entityManager->flush();
    }

    /**
     * Implements DataPersisterInterface::removeData().
     */
    public function removeData($data)
    {
        return $this->removeDataSet(array($data));
    }

    /**
     * Implements DataPersisterInterface::removeDataSet().
     */
    public function removeDataSet(array $dataSet)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');

        foreach ($dataSet as $data) {
            if ($this instanceof DataAccessCheckerInterface) {
                $this->requireDataDeleteAccessGranted($data);
            }

            $entityManager->remove($data);
        }

        $entityManager->flush();
    }

}
