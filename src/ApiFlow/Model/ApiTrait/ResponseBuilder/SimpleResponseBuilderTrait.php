<?php

namespace Th20\ApiFlow\Model\ApiTrait\ReponseBuilder;

use Symfony\Component\HttpFoundation\Response;

use Th20\ApiFlow\Model\ApiInterface\ResponseBuilderInterface;

/**
 * A trait implementation of ResponseBuilderInterface for controller classes.
 */
trait SimpleResponseBuilderTrait
{

    /**
     * Implements ResponseBuilderInterface::buildResponse().
     */
    public function buildResponse($data)
    {
        if ($data instanceof Response) {
            return $data;
        }

        return new Response($data);
    }

}
