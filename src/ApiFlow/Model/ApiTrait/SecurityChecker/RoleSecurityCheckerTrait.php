<?php

namespace Th20\ApiFlow\Model\ApiTrait\SecurityChecker;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Th20\ApiFlow\Model\ApiInterface\SecurityCheckerInterface;

/**
 * A trait implementation of SecurityCheckerInterface for controller classes.
 */
trait RoleSecurityCheckerTrait
{

    private $requestAccessGranted = null;

    private $requestAccessGrantRoles;


    /**
     * Implements SecurityCheckerInterface::isRequestAccessGranted().
     */
    public function isRequestAccessGranted()
    {
        if (!isset($this->requestAccessGranted)) {
            $this->resolveRequestAccessGrants();
        }

        return $this->requestAccessGranted;
    }

    /**
     * Implements SecurityCheckerInterface::requireRequestAccessGranted().
     */
    public function requireRequestAccessGranted()
    {
        if (!$this->isRequestAccessGranted()) {
            throw new AccessDeniedException('Access denied based on role requirements.');
        }
    }

    /**
     * Grants acess if the reqest users has all listed roles.
     */
    protected function grantRequestAccessIfHasRole($roles)
    {
        if (!is_array($roles)) {
            $roles = array($roles);
        }

        $this->requestAccessGrantRoles = $roles;

        return $this;
    }

    private function resolveRequestAccessGrants()
    {
        $user = $this->getUser();
        if (!$user) {
            $this->requestAccessGranted = false;

            return $this;
        }

        foreach ($this->requestAccessGrantRoles as $role) {
            if (!$user->hasRole($role)) {
                $this->requestAccessGranted = false;

                return $this;
            }
        }

        $this->requestAccessGranted = true;

        return $this;
    }

}
