<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataLoader;

use Th20\ApiFlow\Model\ApiInterface\DataLoaderInterface;

/**
 * A trait implementation of DataLoaderInterface for controller classes.
 */
trait JsonToArrayDataLoaderTrait
{

    /**
     * Decodes full Request body as a JSON object.
     */
    public function dataLoadRequestArray()
    {
        $request = $this->getRequest();
        $content = $request->getContent();

        return (array) json_decode($content);
    }

}
