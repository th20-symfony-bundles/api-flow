<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataLoader;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use Th20\ApiFlow\Model\ApiInterface\DataLoaderInterface;

/**
 * A trait implementation of DataLoaderInterface for controller classes.
 */
trait JsonToEntityDataLoaderTrait
{

    /**
     * Deserializes full Request body into a Doctrine mapped entity.
     */
    public function dataLoadRequestEntity($expectedType = null)
    {
        if (empty($expectedType)) {
            $expectedType = 'stdClass';
        }

        $request = $this->getRequest();
        $serializer = $this->get('jms_serializer');

        try {
            return $serializer->deserialize($request->getContent(), $expectedType, 'json');
        } catch (Exception $e) {
            throw new BadRequestHttpException('Input data is invalid.', $e);
        }
    }

}
