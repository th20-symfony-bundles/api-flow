<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataLoader;

use Symfony\Component\HttpFoundation\ParameterBag;

use Th20\ApiFlow\Model\ApiInterface\DataLoaderInterface;

/**
 * A trait implementation of DataLoaderInterface for controller classes.
 */
trait JsonToBagDataLoaderTrait
{

    /**
     * Decodes full Request body as a JSON object into a ParameterBag.
     */
    public function dataLoadRequestBag()
    {
        $request = $this->getRequest();
        $content = $request->getContent();

        $data = (array) json_decode($content);
        return new ParameterBag($data);
    }

}
