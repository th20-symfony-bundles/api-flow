<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataLoader;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Th20\ApiFlow\Model\ApiInterface\DataLoaderInterface;

/**
 * A trait implementation of DataLoaderInterface for controller classes.
 */
trait DoctrineDataLoaderTrait
{

    /**
     * Uses doctrine entity manager to load entity by its type and ID.
     */
    public function dataLoadEntity($entityType, $id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository($entityType);
        if (empty($repo)) {
            throw new BadRequestHttpException();
        }

        $entity = $repo->find($id);
        if ($entity) {
            return $entity;
        }

        throw new NotFoundHttpException();
    }

}
