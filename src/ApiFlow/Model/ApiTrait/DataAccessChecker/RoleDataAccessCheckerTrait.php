<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataAccessChecker;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Th20\ApiFlow\Model\ApiInterface\DataAccessCheckerInterface;

trait RoleDataAccessCheckerTrait
{

    private $roleDataAccessGrants;


    /**
     * Implements DataAccessCheckerInterface::isDataViewAccessGranted().
     */
    public function isDataViewAccessGranted($data)
    {
        return is_array($this->roleDataAccessGrants)
            && in_array('VIEW', $this->roleDataAccessGrants);
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataViewAccessGranted().
     */
    public function requireDataViewAccessGranted($data)
    {
        if (!$this->isDataViewAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on role requirements.');
        }
    }

    /**
     * Implements DataAccessCheckerInterface::isDataCreateAccessGranted().
     */
    public function isDataCreateAccessGranted($data)
    {
        return is_array($this->roleDataAccessGrants)
            && in_array('CREATE', $this->roleDataAccessGrants);
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataCreateAccessGranted().
     */
    public function requireDataCreateAccessGranted($data)
    {
        if (!$this->isDataCreateAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on role requirements.');
        }
    }

    /**
     * Implements DataAccessCheckerInterface::isDataUpdateAccessGranted().
     */
    public function isDataUpdateAccessGranted($data)
    {
        return is_array($this->roleDataAccessGrants)
            && in_array('UPDATE', $this->roleDataAccessGrants);
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataUpdateAccessGranted().
     */
    public function requireDataUpdateAccessGranted($data)
    {
        if (!$this->isDataUpdateAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on role requirements.');
        }
    }

    /**
     * Implements DataAccessCheckerInterface::isDataDeleteAccessGranted().
     */
    public function isDataDeleteAccessGranted($data)
    {
        return is_array($this->roleDataAccessGrants)
            && in_array('DELETE', $this->roleDataAccessGrants);
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataDeleteAccessGranted().
     */
    public function requireDataDeleteAccessGranted($data)
    {
        if (!$this->isDataDeleteAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on role requirements.');
        }
    }

    /**
     * Grants access if the request user has all listed roles.
     */
    protected function grantDataAccessIfHasRole($roles, $access = null)
    {
        if (!is_array($roles)) {
            $roles = array($roles);
        }

        if (!isset($access)) {
            $access = array('view', 'create', 'update', 'delete');
        }

        $user = $this->getUser();
        if (!$user) {
            $this->roleDataAccessGrants = null;

            return $this;
        }

        foreach ($roles as $role) {
            if (!$user->hasRole($role)) {
                $this->roleDataAccessGrants = null;

                return $this;
            }
        }

        $this->roleDataAccessGrants = $access;

        return $this;
    }
}
