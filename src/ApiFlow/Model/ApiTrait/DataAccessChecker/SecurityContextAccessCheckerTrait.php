<?php

namespace Th20\ApiFlow\Model\ApiTrait\DataAccessChecker;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Th20\ApiFlow\Model\ApiInterface\DataAccessCheckerInterface;

trait SecurityContextDataAccessCheckerTrait
{

    /**
     * Implements DataAccessCheckerInterface::isDataViewAccessGranted().
     */
    public function isDataViewAccessGranted($data)
    {
        return $this->get('security.context')->isGranted('view', $data) === true;
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataViewAccessGranted().
     */
    public function requireDataViewAccessGranted($data)
    {
        if (!$this->isDataViewAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on security context configuration.');
        }
    }

    /**
     * Implements DataAccessCheckerInterface::isDataCreateAccessGranted().
     */
    public function isDataCreateAccessGranted($data)
    {
        return $this->get('security.context')->isGranted('create', $data) === true;
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataCreateAccessGranted().
     */
    public function requireDataCreateAccessGranted($data)
    {
        if (!$this->isDataCreateAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on security context configuration.');
        }
    }

    /**
     * Implements DataAccessCheckerInterface::isDataUpdateAccessGranted().
     */
    public function isDataUpdateAccessGranted($data)
    {
        return $this->get('security.context')->isGranted('update', $data) === true;
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataUpdateAccessGranted().
     */
    public function requireDataUpdateAccessGranted($data)
    {
        if (!$this->isDataUpdateAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on security context configuration.');
        }
    }

    /**
     * Implements DataAccessCheckerInterface::isDataDeleteAccessGranted().
     */
    public function isDataDeleteAccessGranted($data)
    {
        return $this->get('security.context')->isGranted('delete', $data) === true;
    }

    /**
     * Implements DataAccessCheckerInterface::requireDataDeleteAccessGranted().
     */
    public function requireDataDeleteAccessGranted($data)
    {
        if (!$this->isDataDeleteAccessGranted($data)) {
            throw new AccessDeniedException('Access to data denied based on security context configuration.');
        }
    }

}
