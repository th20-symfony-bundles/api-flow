<?php

namespace Th20\ApiFlow\Model\ApiInterface;

/**
 * Interface for mandatory data validation.
 */
interface DataValidatorInterface
{

    /**
     * Checks if the provided data passes all validation constraints.
     */
    public function isDataValid($data);

    /**
     * Ensures that the provided data is valid or throws an exception.
     *
     * @throws \Symfony\Component\Validator\Exception\ValidatorException
     */
    public function requireDataValid($data);

}
