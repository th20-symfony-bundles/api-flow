<?php

namespace Th20\ApiFlow\Model\ApiInterface;

/**
 * Interface for mandatory access checks based on request properties.
 */
interface SecurityCheckerInterface
{

    /**
     * Checks if an access to the current request was granted.
     *
     * @return boolean
     */
    public function isRequestAccessGranted();

    /**
     * Ensures that the request access is granted or throws an exception.
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function requireRequestAccessGranted();

}
