<?php

namespace Th20\ApiFlow\Model\ApiInterface;

interface ControllerInterface extends
    DataAccessCheckerInterface,
    DataLoaderInterface,
    DataPersisterInterface,
    DataValidatorInterface,
    ResponseBuilderInterface,
    SecurityCheckerInterface
{
}
