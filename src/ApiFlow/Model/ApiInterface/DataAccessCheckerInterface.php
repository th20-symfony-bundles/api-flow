<?php

namespace Th20\ApiFlow\Model\ApiInterface;

/**
 * Interface for mandatory access checks for processed data.
 */
interface DataAccessCheckerInterface
{

    /**
     * Checks if user has access to view data.
     */
    public function isDataViewAccessGranted($data);

    /**
     * Ensures that user has view access or throws an exception.
     */
    public function requireDataViewAccessGranted($data);

    /**
     * Checks if user has access to create data.
     */
    public function isDataCreateAccessGranted($data);

    /**
     * Ensures that user has create access or throws an exception.
     */
    public function requireDataCreateAccessGranted($data);

    /**
     * Checks if user has access to update data.
     */
    public function isDataUpdateAccessGranted($data);

    /**
     * Ensures that user has update access or throws an exception.
     */
    public function requireDataUpdateAccessGranted($data);

    /**
     * Checks if user has access to delete data.
     */
    public function isDataDeleteAccessGranted($data);

    /**
     * Ensures that user has delete access or throws an exception.
     */
    public function requireDataDeleteAccessGranted($data);

}
