<?php

namespace Th20\ApiFlow\Model\ApiInterface;

/**
 * Interface for secure data persistance.
 *
 * Interface implementations must ensure that the persisted data is valid
 * and appropriate access has been granted.
 */
interface DataPersisterInterface
{

    /**
     * Persists data to the data storage.
     */
    public function persistData($data);

    /**
     * Persists multiple data objects to data storage.
     */
    public function persistDataSet(array $dataSet);

    /**
     * Removes data from the data storage.
     */
    public function removeData($data);

    /**
     * Removes multiple data objects from data storage.
     */
    public function removeDataSet(array $data);

}
