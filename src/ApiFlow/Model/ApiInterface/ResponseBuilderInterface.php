<?php

namespace Th20\ApiFlow\Model\ApiInterface;

/**
 * Interface for action response builders.
 */
interface ResponseBuilderInterface
{

    /**
     * Transforms data into a Response object.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function buildResponse($data);

}
